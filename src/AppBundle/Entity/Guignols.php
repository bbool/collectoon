<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Guignols
 *
 * @ORM\Table(name="guignols")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GuignolsRepository")
 */
class Guignols
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="date", type="string", length=200)
     */
    private $date;

    /**
     * @ORM\Column(name="commentaires", type="string", length=200)
     */
    private $commentaires;

    public function getId()
    {
        return $this->id;
    }

    public function setId($newId)
    {
        $this->id = $newId;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($newDate)
    {
        $this->date = $newDate;
    }

    public function getCommentaires()
    {
        return $this->$commentaires;
    }

    public function setCommentaires($newCommentaires)
    {
        $this->commentaires = $newCommentaires;
    }
}

